#!flask/bin/python
from flask import Flask, render_template
import redis
import configargparse
import json
import logging

app = Flask(__name__)

_args = None
_cache = None
_key_all = "kfailb_all"
_key_line = "kfailb_line_{}"

@app.route('/')
def index():
    resp = _cache.get(_key_all)
    if resp:
        response = app.response_class(
            response=resp.decode('utf-8'),
            mimetype='application/json'
        )

        resp = json.loads(resp.decode('utf-8'))
        return render_template('list.html', data=resp)

    return ""

@app.route('/<int:line>')
def update_task(line):
    resp = _cache.get(_key_line.format(line))
    reply = ""
    if resp:
        resp = json.loads(resp.decode('utf-8'))
        return render_template('list.html', data=resp)
    return reply

def parse_args():
    """ Argparse stuff happens here. """
    parser = configargparse.ArgumentParser(prog='k-fail-b')
    parser.add_argument('-r', '--redis-host', dest="redis_host", action="store", env_var="KFAILBACKEND_REDIS")

    global _args
    _args = parser.parse_args()

def init_cache():
    global _cache
    _cache = redis.Redis(host=_args.redis_host, port=6379, db=0)


if __name__ == '__main__':
    parse_args()
    init_cache()

    app.run(debug=True, host='0.0.0.0')

