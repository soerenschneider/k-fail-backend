FROM python:3.7-slim

COPY requirements.txt /opt/kfailbackend/requirements.txt
WORKDIR /opt/kfailbackend
RUN pip install --no-cache-dir -r requirements.txt

COPY templates /opt/kfailbackend/templates/
COPY kfailbackend.py /opt/kfailbackend/

RUN useradd toor
USER toor

CMD ["python3", "kfailbackend.py"]
