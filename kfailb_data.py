
from dataclasses import dataclass, field
from typing import List, Dict, Any
import datetime

@dataclass
class Station:
    station: str
    time: str

    def __repr__(self):
        return f'{self.station} ({self.time})'


@dataclass
class Notification:
    what: str
    stations: List[Station]
    direction: str = field(init=False)

    def __post_init__(self):
        if len(self.stations):
            self.direction = "{} -> {}".format(self.stations[0].station, self.stations[len(self.stations)-1].station)
        else:
            self.direction = ""

    def __repr__(self):
        return f'{self.line} ({self.direction}): {self.what}\n{self.stations}\n'

@dataclass
class Line:
    line: int
    notifications: List[Notification]

@dataclass
class Notifications:
    lines: Dict[int, Line]
    fetch_date: Any = datetime.time.now()
